#include <Encoder.h>
 
 //Declaração pins e variáveis

//Sonar Pin(5,4,7,6,2,10)
char frenteSonarTrig = 5;
char frenteSonarEcho = 4;
char esquerdaSonarTrig = 7;
char esquerdaSonarEcho = 6;
char direitaSonarTrig = 2;
char direitaSonarEcho = 10;

//Sensor chama pin(A5)
int sensorChama = A5;

//Pin Motor
//A(3,12)
int motorAdir = 12;
int motorApwm = 3;
Encoder LeftEncoder(A0,A1);

//B(11,13)
int motorBdir = 13;
int motorBpwm = 11;
Encoder RightEncoder(A2,A3);

//Fan pin(A2)
int fan = A2;

//LED vela detetada
int LED = 8;

//Botao Start/Stop
int botaoStart = A3;
int botaoStop = A4;

//Declaração de variáveis utilizadas para controlo
double frenteVal, esquerdaVal, direitaVal, distanciaVal,cronometro=0;
int valorChama , duracao, sensorChamaVal = 0, count = 0, motorAspeed, motorBspeed, estadoRobo=1;
 
#include <SoftwareSerial.h>
SoftwareSerial Bluetooth(0,1); // RX, TX
int Data; // the data received

void setup() {
  Serial.begin(9600);
  Bluetooth.begin(9600);
  
  //Sonar frente
  pinMode(frenteSonarTrig, OUTPUT);
  pinMode(frenteSonarEcho, INPUT);

  //Sonar esquerda
  pinMode(esquerdaSonarTrig, OUTPUT);
  pinMode(esquerdaSonarEcho, INPUT);

  //Sonar direita
  pinMode(direitaSonarTrig, OUTPUT);
  pinMode(direitaSonarEcho, INPUT);

  //Fan
  pinMode(fan, OUTPUT);

  //Motores direito e esquerdo
  pinMode(motorAdir, OUTPUT);
  pinMode(motorApwm, OUTPUT);
  pinMode(motorBdir, OUTPUT);
  pinMode(motorBpwm, OUTPUT);

  //LED vela detetada
  pinMode(LED, OUTPUT);

  
  //Botao start/stop
  pinMode(botaoStart, INPUT);
  pinMode(botaoStop, INPUT);
  
}


void frente(int LeftMotorSpeed, int LeftMotorDegrees, int RightMotorSpeed, int RightMotorDegrees) {
 if (LeftMotorDegrees >= 0)
  {
  //Motor A
  digitalWrite(motorAdir, LOW);
  analogWrite(motorApwm, LeftMotorSpeed);
 }
  else
  {
   //Motor A
  digitalWrite(motorAdir, HIGH);
  analogWrite(motorApwm, LeftMotorSpeed);
  } 
  if (RightMotorDegrees >= 0)
  {
  //Motor B
  digitalWrite(motorBdir, LOW);
  analogWrite(motorBpwm, motorBspeed);
   }
  else
  {
 //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);
  } 
  //delay(duracao);
}


void viraEsquerda(int LeftMotorSpeed, int LeftMotorDegrees, int RightMotorSpeed, int RightMotorDegrees) {

 if (LeftMotorDegrees >= 0)
  {
  //Motor A
  digitalWrite(motorAdir, LOW);
  analogWrite(motorApwm, LeftMotorSpeed);
 }
  else
  {
   //Motor A
  digitalWrite(motorAdir, LOW);
  analogWrite(motorApwm, LeftMotorSpeed);
  } 
  if (RightMotorDegrees >= 0)
  {
  //Motor B
  digitalWrite(motorBdir, LOW);
  analogWrite(motorBpwm, motorBspeed);
   }
  else
  {
 //Motor B
  digitalWrite(motorBdir, LOW);
  analogWrite(motorBpwm, motorBspeed);
  } 
}

void viraDireita(int LeftMotorSpeed, int LeftMotorDegrees, int RightMotorSpeed, int RightMotorDegrees) {

if (LeftMotorDegrees >= 0)
  {
  //Motor A
  digitalWrite(motorAdir, HIGH);
  analogWrite(motorApwm, LeftMotorSpeed);
 }
  else
  {
   //Motor A
  digitalWrite(motorAdir, HIGH);
  analogWrite(motorApwm, LeftMotorSpeed);
  } 
  if (RightMotorDegrees >= 0)
  {
  //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);
   }
  else
  {
 //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);
  }
}



double lerSonar(char sonarTrig, char sonarEcho) {
  //Limpa o trigger
  digitalWrite(sonarTrig, LOW);
  delayMicroseconds(2);

  //Liga o trigger por 10 milisegundos
  digitalWrite(sonarTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(sonarTrig, LOW);

  //Recebe a informação (eccho)
  distanciaVal = pulseIn(sonarEcho, HIGH) * 0.034 / 2;

  return distanciaVal;
}

void roboStart(){
if(analogRead(botaoStart)==0){
      estadoRobo=1;
      }
}
void roboStop(){    
if(analogRead(botaoStop)==1){
      estadoRobo=0;
      }
}
void loop() {
   // roboStart();
   // roboStop();
  
  Data=Bluetooth.read();
  switch(estadoRobo){
    case 0 :
      break;
    case 1 :
      frente(255,100,255,100);     
      if(lerSonar(frenteSonarTrig, frenteSonarEcho) < 20){
       estadoRobo=2;
       resultados();
        }else {
          estadoRobo=1;
          resultados();
         }

          if(lerSonar(frenteSonarTrig, frenteSonarEcho)>20){
            if(lerSonar(direitaSonarTrig,direitaSonarEcho) > 80){
              estadoRobo=7;
              } else if(lerSonar(esquerdaSonarTrig,esquerdaSonarEcho) > 40){
                frente(200,0,200,0);
                 delay(550);
                 estadoRobo=6;
                 resultados();
                }
               
            } 
            
          
          /*
       if(lerSonar(frenteSonarTrig, frenteSonarEcho)>20){        
               
        if(lerSonar(direitaSonarTrig,direitaSonarEcho) > 80){
        frente(127,127);
        delay(550);
        estadoRobo=7;
        resultados(); 
        break;       
       } 
       else if(lerSonar(esquerdaSonarTrig,esquerdaSonarEcho) > 80 && (direitaSonarTrig,direitaSonarEcho) < 20){
        frente(127,127);
        delay(550);
        estadoRobo=6;
        resultados(); 
        break;       
       }  
       }
        
        else if(lerSonar(frenteSonarTrig, frenteSonarEcho)<20){
          if (lerSonar(esquerdaSonarTrig, esquerdaSonarEcho)>80){
        estadoRobo=6;
        resultados();
        break;
          }
       }else if (lerSonar(direitaSonarTrig, direitaSonarEcho)>80){
          estadoRobo=7;
          resultados();
          break;
          }
       */
       
      
        
      
      break;
    case 2 :
      roboPara();
      delay(500);

      if(lerSonar(direitaSonarTrig, direitaSonarEcho) > 20){
        // roda direita
       
        estadoRobo=7;
        }else{ // direita esta ocupada
          if(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) > 20){
            // roda esquerda
            estadoRobo=6;        

            } 
          }

      
      /*
      if(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) < 20){ //mudou-se de direita para esquerda
        
        estadoRobo=7;
        resultados();
        break;
        }
      else if(lerSonar(direitaSonarTrig, direitaSonarEcho) < 20) {
        estadoRobo=6;
        resultados();
        break;
      } 
      if(lerSonar(frenteSonarTrig, frenteSonarEcho)<20){
          if (lerSonar(direitaSonarTrig, direitaSonarEcho)<20){
      estadoRobo=6;
          } else if(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho)<20) {
            estadoRobo=7;
            resultados();
            break;
          } else{
        estadoRobo=1;
        resultados();
            break;
  }
      }*/
      break;
  
 
    case 4 :
       roboPara();
      if(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) > 20){ 
        estadoRobo=6;
        resultados();
        }
      else  if(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) < 20){ //esquerda continua ocupado, em vez da direita
        estadoRobo=1;
        resultados();
      } 
      
      break;
    /*case 5 :
    //  rodaCompleta(200,200,2400);
      delay(800);
      estadoRobo=1;
      resultados();
   break;*/
    case 6 :
      viraEsquerda(200,90,200,90);
      delay(800);
      estadoRobo=1;
      frente(200,0,200,0);
      delay(1000);
      resultados();
      break;
    case 7 :
       roboPara();
       viraDireita(200,90,200,90);
       delay(800);
       frente(200,0,200,0);
       delay(1000);
       estadoRobo=1;
       resultados();  
       break;

    case 8 :
      frente(200,0,200,0);
    while(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) < 20){
      cronometro = millis();
      }
    if(3000-cronometro<0){
      roboPara();
      //rodaCompleta(127,127,400);
      estadoRobo=1;  
      resultados();
    }
    break;
  }
}

void resultados(){
    //Movimento do robo
   Serial.println("Distancia: \t Frente \t Esquerda \t Direita");  
  Serial.print("\t \t");
  Serial.print(lerSonar(frenteSonarTrig, frenteSonarEcho));
  Serial.print("\t \t");
  Serial.print(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho));
  Serial.print("\t \t");
  Serial.println(lerSonar(direitaSonarTrig, direitaSonarEcho));
    //Bluetooth
    Bluetooth.println("Distancia: \t Frente \t Esquerda \t Direita");
    Bluetooth.print("\t \t");
    Bluetooth.print(lerSonar(frenteSonarTrig, frenteSonarEcho));
    Bluetooth.print("\t \t");
    Bluetooth.print(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho));
    Bluetooth.print("\t \t");
    Bluetooth.println(lerSonar(direitaSonarTrig, direitaSonarEcho));
  
  //Funcao Chama
 /*
  Serial.print("Chama: ");
  //Bluetooth
  Bluetooth.print("Chama: ");
  Serial.println(analogRead(sensorChama));
  //Bluetooth
  Bluetooth.println(analogRead(sensorChama));
 detetaChama();
  //botoes

  */
  Serial.print("Estado de Robo: ");
  Serial.println(estadoRobo);

 long LeftEncoderRead, RightEncoderRead;
  LeftEncoderRead = LeftEncoder.read();
  RightEncoderRead = RightEncoder.read();
  Serial.print("Encoder:");
   Serial.println(LeftEncoderRead && RightEncoderRead);
  }
void roboPara() {
  //Motor A
    analogWrite(motorApwm, 0);
  //Motor B
    analogWrite(motorBpwm, 0);

  delay(100);
}

/*
void detetaChama() {
if (analogRead(sensorChama) > 400) {
    roboPara();
    ledChama();
    while(analogRead(sensorChama) > 400){
    digitalWrite(fan, HIGH);
    }
    count++;
     Serial.println(count);
      //Bluetooth
      Bluetooth.println(count);
  }
  
}


void ledChama() {
  if (analogRead(sensorChama) > 400) {
   
    digitalWrite(LED, HIGH);
    delay(3000);
    digitalWrite(LED, LOW);
  
}
}
*/
