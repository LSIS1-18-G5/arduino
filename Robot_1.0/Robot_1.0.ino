//Declaração pins e variáveis

//Sonar Pin(5,4,7,6,2,10)
char frenteSonarTrig = 5;
char frenteSonarEcho = 4;
char esquerdaSonarTrig = 7;
char esquerdaSonarEcho = 6;
char direitaSonarTrig = 8;
char direitaSonarEcho = 10;

//Sensor chama pin(A5)
int sensorChama = A5;

//Pin Motor
//A(3,12)
int motorAdir = 12;
int motorApwm = 3;

//B(11,13)
int motorBdir = 13;
int motorBpwm = 11;

//Fan pin(A2)
int fan = A2;

//LED vela detetada
int LED = A3;

//Botao Start/Stop
int botaoStart = 2;
int botaoStop = 9;

//Declaração de variáveis utilizadas para controlo
int valorChama , duracao, sensorChamaVal = 0, count = 0, motorAspeed, motorBspeed, estadoRobo = 0, sensorVal, distanciaVal, Data;
String robot = "OFF";

#include <SoftwareSerial.h>
SoftwareSerial Bluetooth(0, 1); // RX, TX

void setup() {
  Serial.begin(9600);
  Bluetooth.begin(9600);

  //Sonar frente
  pinMode(frenteSonarTrig, OUTPUT);
  pinMode(frenteSonarEcho, INPUT);

  //Sonar esquerda
  pinMode(esquerdaSonarTrig, OUTPUT);
  pinMode(esquerdaSonarEcho, INPUT);

  //Sonar direita
  pinMode(direitaSonarTrig, OUTPUT);
  pinMode(direitaSonarEcho, INPUT);

  //Fan
  pinMode(fan, OUTPUT);

  //Motores direito e esquerdo
  pinMode(motorAdir, OUTPUT);
  pinMode(motorApwm, OUTPUT);
  pinMode(motorBdir, OUTPUT);
  pinMode(motorBpwm, OUTPUT);

  //LED vela detetada
  pinMode(LED, OUTPUT);


  //Botao start/stop
  pinMode(botaoStart, INPUT);
  pinMode(botaoStop, INPUT);

}

//Função anda para a frente
void frente(int motorAspeed, int motorBspeed) {

  //Motor A
  digitalWrite(motorAdir, LOW);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);
}

//Função vira esquerda
void viraEsquerda(int motorAspeed, int motorBspeed, int duracao) {

  //Motor A
  digitalWrite(motorAdir, LOW);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, LOW);
  analogWrite(motorBpwm, motorBspeed);

  delay(duracao);
}

//Função vira direita
void viraDireita(int motorAspeed, int motorBspeed, int duracao) {

  //Motor A
  digitalWrite(motorAdir, HIGH);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);

  delay(duracao);
}

//Ler distância do Sonar
double lerSonar(char sonarTrig, char sonarEcho) {
  //Limpa o trigger
  digitalWrite(sonarTrig, LOW);
  delayMicroseconds(2);

  //Liga o trigger por 10 milisegundos
  digitalWrite(sonarTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(sonarTrig, LOW);

  //Recebe a informação (eccho)
  distanciaVal = pulseIn(sonarEcho, HIGH) * 0.034 / 2;
  //Retorna 
  return distanciaVal;
}


void loop() {
  Data = Bluetooth.read();
  switch (estadoRobo) {
    case 0:
      while (robot == "OFF") { //enquanto estado do robo OFF
        detetaChama();//deteta chama logo no inicio
        frente(0,0); //pára o robot
        if (digitalRead(botaoStart) == LOW) {//Verifica botao Start
          robot = "ON";//muda estado robo para ON
        }
      }

      if (robot == "ON") {// Se estado do robo igual a ON, estado passa para 1
        estadoRobo = 1;
      }
      
      break;
    case 1://Avalia o sensor da frente
      if (digitalRead(botaoStop) == LOW) { //verfica botao Stop
        robot = "OFF";//muda estado do robo para OFF
        estadoRobo = 0; //muda estado robo para 0
        frente(0,0); //pára o robot
        break;
      } 
      resultados();//imprime resultados
      frente(200, 200);//anda para a frente a 200
      if ( lerSonar(frenteSonarTrig, frenteSonarEcho)  < 20) {  //frente ocupada se a menos de 20
        detetaChama();// verifica existencia de chama
        estadoRobo = 4; //muda estado do robo para 4
      }
      else {//mantem estado do robo a 1
        detetaChama();//verfica se existe chama
        estadoRobo = 1;//mantem o estado do robo
      }


      if (lerSonar(frenteSonarTrig, frenteSonarEcho) > 20) { //Se sensor da frente maior que 20, avalia existencia de portas
        if ( lerSonar(direitaSonarTrig, direitaSonarEcho) > 40) { //Se a direita for maior que 40, encontrou porta à direita
          detetaChama();//Procura chama
          estadoRobo = 2;//muda estado do robo para 2
          break;
        }  else{
          detetaChama();//Procura chama
          estadoRobo = 1;//mantem estado do robo
        }
      }

      break;

    case 2: // VIRAR DIREITA
      if (digitalRead(botaoStop) == LOW) {//verifica botao stop
        robot = "OFF";//muda estado do robo para OFF
        estadoRobo = 0;
        frente(0,0); //pára o robot
        break;
      }
      detetaChama();
      resultados();
      frente(127, 127);
      delay(200);
      roboPara();
      viraDireita(250, 250, 1500);
      frente(200, 200);
      delay(1000);
      estadoRobo = 1; //volta a avaliar a frente
      break;
    case 3: // VIRAR ESQUERDA
      if (digitalRead(botaoStop) == LOW) { //verifica botao stop
        robot = "OFF";//muda estado do robo para OFF
        estadoRobo = 0;
        frente(0,0); //pára o robot
        break;
      }
      detetaChama();//Procura a chama
      resultados(); //imprime resultados
      frente(127, 127);//frente 
      delay(200);
      roboPara();//para robo
      viraEsquerda(250, 250, 1250);//rotação 90º para a esquerda
      frente(200, 200);//frente
      delay(1000);
      estadoRobo = 1; //volta a avaliar a frente
      break;
    case 4: // SONAR FRENTE
      if (digitalRead(botaoStop) == LOW) {
        robot = "OFF";
        estadoRobo = 0;
        frente(0,0); //pára o robot
        break;
      }
      resultados();

      if (lerSonar(direitaSonarTrig, direitaSonarEcho) > 40) {
        // roda direita
        detetaChama();
        estadoRobo = 2;
      } else { // direita esta ocupada
        if (lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) > 40) {
          // roda esquerda
          detetaChama();
          estadoRobo = 3;

        }
      }
      break;

  }
  Serial.print(estadoRobo);
  delay(175);
}

void resultados() {
  //Movimento do robo
  /*Serial.println("Distancia: \t Frente \t Esquerda \t Direita");
  Serial.print("\t \t");
  Serial.print(lerSonar(frenteSonarTrig, frenteSonarEcho));
  Serial.print("\t \t");
  Serial.print(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho));
  Serial.print("\t \t");
  Serial.println(lerSonar(direitaSonarTrig, direitaSonarEcho));
  //Bluetooth
  Bluetooth.println("Distancia: \t Frente \t Esquerda \t Direita");
  Bluetooth.print("\t \t");
  Bluetooth.print(lerSonar(frenteSonarTrig, frenteSonarEcho));
  Bluetooth.print("\t \t");
  Bluetooth.print(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho));
  Bluetooth.print("\t \t");
  Bluetooth.println(lerSonar(direitaSonarTrig, direitaSonarEcho));
  Serial.print("Estado de Robo: ");
  Serial.println(estadoRobo);

  //Funcao Chama

  Serial.print("Chama: ");
  //Bluetooth
  Bluetooth.print("Chama: ");
  Serial.println(analogRead(sensorChama));
  //Bluetooth
  Bluetooth.println(analogRead(sensorChama));*/

}
void roboPara() {
  //Motor A
  analogWrite(motorApwm, 0);
  //Motor B
  analogWrite(motorBpwm, 0);

 // delay(100);
}
//Função deteta chama

void detetaChama() {
  if (analogRead(sensorChama) > 500) {//se detetar valor acima de 600
    roboPara();//para o robo
    ledChama();//liga o led
    Serial.print("5"); //robot detetou chama
    do {
      
      digitalWrite(fan, 255);
      delay(5000);
    } while (analogRead(sensorChama) > 500);//enquanto detetar valor acima de 600,deixa a ventoinha ligada
    Serial.print("0"); //robot apagou a vela
  }
  else  if (analogRead(sensorChama) <= 500) {//senão ventoinha e led desligados
    digitalWrite(LED, LOW);//Led desliga
    digitalWrite(fan, LOW);//Fan desliga
  }
}

//Função acender led quando deteta chama

void ledChama() {
  if (analogRead(sensorChama) > 500) {
    digitalWrite(LED, HIGH);
    delay(3000);
    digitalWrite(LED, LOW);

  }
}