//Declaração pins e variáveis

//Sonar Pin(5,4,7,6,2,10)
char frenteSonarTrig = 5;
char frenteSonarEcho = 4;
char esquerdaSonarTrig = 7;
char esquerdaSonarEcho = 6;
char direitaSonarTrig = 2;
char direitaSonarEcho = 10;

//Sensor chama pin(A5)
int sensorChama = A5;

//Pin Motor
//A(3,12)
int motorAdir = 12;
int motorApwm = 3;

//B(11,13)
int motorBdir = 13;
int motorBpwm = 11;

//Fan pin(A2)
int fan = A2;

//LED vela detetada
int LED = 8;

//Botao Start/Stop
int botaoStart = A3;
int botaoStop = A4;

//Declaração de variáveis utilizadas para controlo
double frenteVal, esquerdaVal, direitaVal, distanciaVal,cronometro=0;
int valorChama , duracao, sensorChamaVal = 0, count = 0, motorAspeed, motorBspeed, estadoRobo=1, direita1,esquerda1,frente1;
 
#include <SoftwareSerial.h>
SoftwareSerial Bluetooth(0,1); // RX, TX
int Data; // the data received

void setup() {
  Serial.begin(9600);
  Bluetooth.begin(9600);
  
  //Sonar frente
  pinMode(frenteSonarTrig, OUTPUT);
  pinMode(frenteSonarEcho, INPUT);

  //Sonar esquerda
  pinMode(esquerdaSonarTrig, OUTPUT);
  pinMode(esquerdaSonarEcho, INPUT);

  //Sonar direita
  pinMode(direitaSonarTrig, OUTPUT);
  pinMode(direitaSonarEcho, INPUT);

  //Fan
  pinMode(fan, OUTPUT);

  //Motores direito e esquerdo
  pinMode(motorAdir, OUTPUT);
  pinMode(motorApwm, OUTPUT);
  pinMode(motorBdir, OUTPUT);
  pinMode(motorBpwm, OUTPUT);

  //LED vela detetada
  pinMode(LED, OUTPUT);

  
  //Botao start/stop
  pinMode(botaoStart, INPUT);
  pinMode(botaoStop, INPUT);
  
}


void frente(int motorAspeed, int motorBspeed) {

  //Motor A
  digitalWrite(motorAdir, LOW);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);

<<<<<<< Updated upstream
  //delay(duracao);
=======
>>>>>>> Stashed changes
}




void viraEsquerda(int motorAspeed, int motorBspeed, int duracao) {

  //Motor A
  digitalWrite(motorAdir, LOW);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, LOW);
  analogWrite(motorBpwm, motorBspeed);

  delay(duracao);
}

void viraDireita(int motorAspeed, int motorBspeed, int duracao) {

  //Motor A
  digitalWrite(motorAdir, HIGH);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);

  delay(duracao);
}

void gira180(int motorAspeed, int motorBspeed, int duracao) {

  //Motor A
  digitalWrite(motorAdir, HIGH);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);

  delay(duracao);
}

double lerSonar(char sonarTrig, char sonarEcho) {
  //Limpa o trigger
  digitalWrite(sonarTrig, LOW);
  delayMicroseconds(2);

  //Liga o trigger por 10 milisegundos
  digitalWrite(sonarTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(sonarTrig, LOW);

  //Recebe a informação (eccho)
  distanciaVal = pulseIn(sonarEcho, HIGH) * 0.034 / 2;

  return distanciaVal;
}

void roboStart(){
if(analogRead(botaoStart)==0){
      estadoRobo=1;
      }
}
void roboStop(){    
if(analogRead(botaoStop)==1){
      estadoRobo=0;
      }
}
void loop() {
   // roboStart();
   // roboStop();
  frente1=lerSonar(frenteSonarTrig, frenteSonarEcho);
  esquerda1=lerSonar(esquerdaSonarTrig, esquerdaSonarEcho);
  direita1=lerSonar(direitaSonarTrig, direitaSonarEcho);
  Data=Bluetooth.read();
  switch(estadoRobo){
    case 0 :
      break;
    case 1 :
<<<<<<< Updated upstream
      frente(200,200);     
      if(lerSonar(frenteSonarTrig, frenteSonarEcho) < 20){
        estadoRobo=2;
        }
        
      /*else if(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) > 20){
        estadoRobo=8;
       }
       else if(lerSonar(direitaSonarTrig,direitaSonarEcho) > 15){
        estadoRobo=7;
        
       }*/
       
      else{
=======
      frente(200,200);
      delay(1000);  
      if(frente1 < 25){
        roboPara();
        estadoRobo=2;
        }else{
        estadoRobo=1;
      }
      if(frente1 > 25){
       if(direita1 > 80){
        frente(200,200);
        delay(1000);
        estadoRobo=7;
        
       }
       else{
>>>>>>> Stashed changes
        estadoRobo=1;
      }
       }
      
      break;
    case 2 :
      if(direita1 < 20){
        estadoRobo=4;
        }
      else{
        estadoRobo=3;  
        
      }
      break;
    case 3 :
      viraDireita(200,200,1000);
 
      estadoRobo=1;
     break; 
    case 4 :
       roboPara();
      if(esquerda1> 20){
        estadoRobo=6;
        }
      else  if(esquerda1 < 20){
        estadoRobo=5;  
      }
      break;
    case 5 :
      gira180(200,200,200);

      estadoRobo=1;
   break;
    case 6 :
      viraEsquerda(200,200,1000);

      estadoRobo=1;
      break;
    case 7 :
       roboPara();
       viraDireita(200,200,1000);
        delay(1000);
        frente(200,200);
        delay(1000);
        estadoRobo=1;  
        break;

<<<<<<< Updated upstream
    case 8 :
=======
   /* case 8 :
>>>>>>> Stashed changes
    frente(200,200);
    while(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) < 20){
      cronometro = millis();
      }
    if(3000-cronometro<0){
      roboPara();
      gira180(127,127,350);
      estadoRobo=1;  
    }
    break;*/
  }

  //Movimento do robo
   Serial.println("Distancia: \t Frente \t Esquerda \t Direita");  
  Serial.print("\t \t");
  Serial.print(frente1);
  Serial.print("\t \t");
  Serial.print(esquerda1);
  Serial.print("\t \t");
  Serial.println(direita1);
    //Bluetooth
    Bluetooth.println("Distancia: \t Frente \t Esquerda \t Direita");
    Bluetooth.print("\t \t");
    Bluetooth.print(frente1);
    Bluetooth.print("\t \t");
    Bluetooth.print(esquerda1);
    Bluetooth.print("\t \t");
    Bluetooth.println(direita1);
  
  //Funcao Chama
 
  Serial.print("Chama: ");
  //Bluetooth
  Bluetooth.print("Chama: ");
  Serial.println(analogRead(sensorChama));
  //Bluetooth
  Bluetooth.println(analogRead(sensorChama));
 detetaChama();
  //botoes

  
  Serial.print("Estado de Robo: ");
  Serial.println(estadoRobo);

 

}


void roboPara() {
  //Motor A
    analogWrite(motorApwm, 0);
  //Motor B
    analogWrite(motorBpwm, 0);

  delay(100);
}

void detetaChama() {
if (analogRead(sensorChama) > 400) {
    roboPara();
    ledChama();
    while(analogRead(sensorChama) > 400){
    digitalWrite(fan, HIGH);
    }
    count++;
     Serial.println(count);
      //Bluetooth
      Bluetooth.println(count);
  }
  
}


void ledChama() {
  if (analogRead(sensorChama) > 400) {
   
    digitalWrite(LED, HIGH);
    delay(3000);
    digitalWrite(LED, LOW);
  
}
}
