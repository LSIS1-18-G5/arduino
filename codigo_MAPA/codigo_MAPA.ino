//Declaração pins e variáveis

//Sonar Pin(5,4,7,6,2,10)
char frenteSonarTrig = 5;
char frenteSonarEcho = 4;
char esquerdaSonarTrig = 7;
char esquerdaSonarEcho = 6;
char direitaSonarTrig = 2;
char direitaSonarEcho = 10;

//Sensor chama pin(A5)
int sensorChama = A5;

//Pin Motor
//A(3,12)
int motorAdir = 12;
int motorApwm = 3;

//B(11,13)
int motorBdir = 13;
int motorBpwm = 11;

//Fan pin(A2)
int fan = A2;

//LED vela detetada
int LED = 8;

//Botao Start/Stop
int botaoStart = A3;
int botaoStop = A4;

//Declaração de variáveis utilizadas para controlo
double frenteVal, esquerdaVal, direitaVal, distanciaVal,cronometro=0;
int valorChama , duracao, sensorChamaVal = 0, count = 0, motorAspeed, motorBspeed, estadoRobo=0, fEsquerda=0, fDireita=0, vc=0, quarto3=0, Esq=0, dir=0;
String robot="OFF";

#include <SoftwareSerial.h>
SoftwareSerial Bluetooth(0,1); // RX, TX
int Data; // the data received

void setup() {
  Serial.begin(9600);
  Bluetooth.begin(9600);
  
  //Sonar frente
  pinMode(frenteSonarTrig, OUTPUT);
  pinMode(frenteSonarEcho, INPUT);

  //Sonar esquerda
  pinMode(esquerdaSonarTrig, OUTPUT);
  pinMode(esquerdaSonarEcho, INPUT);

  //Sonar direita
  pinMode(direitaSonarTrig, OUTPUT);
  pinMode(direitaSonarEcho, INPUT);

  //Fan
  pinMode(fan, OUTPUT);

  //Motores direito e esquerdo
  pinMode(motorAdir, OUTPUT);
  pinMode(motorApwm, OUTPUT);
  pinMode(motorBdir, OUTPUT);
  pinMode(motorBpwm, OUTPUT);

  //LED vela detetada
  pinMode(LED, OUTPUT);

  
  //Botao start/stop
  pinMode(botaoStart, INPUT);
  pinMode(botaoStop, INPUT);
  
}

void detetaChama() {
if (analogRead(sensorChama) > 400) {
    frente(0,0);
    ledChama();
    while(analogRead(sensorChama) > 400){
    digitalWrite(fan, HIGH);
    }
    count++;
     Serial.println(count);
      //Bluetooth
      Bluetooth.println(count);
  }
  
}


void ledChama() {
  if (analogRead(sensorChama) > 400) {
   
    digitalWrite(LED, HIGH);
    delay(3000);
    digitalWrite(LED, LOW);
  
}
}

void frente(int motorAspeed, int motorBspeed) {

  //Motor A
  digitalWrite(motorAdir, LOW);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);

  //delay(duracao);
}

void re(int motorAspeed, int motorBspeed, int duracao) {
  
  //Motor A
  digitalWrite(motorAdir, HIGH);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, LOW);
  analogWrite(motorBpwm, motorBspeed);

  delay(duracao);
}



void viraEsquerda(int motorAspeed, int motorBspeed, int duracao) {

  //Motor A
  digitalWrite(motorAdir, LOW);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, LOW);
  analogWrite(motorBpwm, motorBspeed);

  delay(duracao);
}

void viraDireita(int motorAspeed, int motorBspeed, int duracao) {

  //Motor A
  digitalWrite(motorAdir, HIGH);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);

  delay(duracao);
}

void rodaCompleta(int motorAspeed, int motorBspeed, int duracao) {

  //Motor A
  digitalWrite(motorAdir, HIGH);
  analogWrite(motorApwm, motorAspeed);

  //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, motorBspeed);

  delay(duracao);
}

double lerSonar(char sonarTrig, char sonarEcho) {
  //Limpa o trigger
  digitalWrite(sonarTrig, LOW);
  delayMicroseconds(2);

  //Liga o trigger por 10 milisegundos
  digitalWrite(sonarTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(sonarTrig, LOW);

  //Recebe a informação (eccho)
  distanciaVal = pulseIn(sonarEcho, HIGH) * 0.034 / 2;

  return distanciaVal;
}

/*void roboStart(){
if(analogRead(botaoStart)==0){
      estadoRobo=1;
      }
}
void roboStop(){    
if(analogRead(botaoStop)==1){
      estadoRobo=0;
      }
}
*/
void loop() {
   // roboStart();
   // roboStop();
  
 
  Data=Bluetooth.read();
  
switch(estadoRobo){
  case 0:

   while (robot=="OFF"){
    if(analogRead(botaoStart)==0){
      robot="ON";
      }    
    }
    if(robot="ON"){
      estadoRobo=1;
      }
break;
  case 1:
if(analogRead(botaoStop)==1){
      robot="OFF";
      estadoRobo=0;
   }
  
    frente(200,200);
//-------------------------------------------------
      if(lerSonar(frenteSonarTrig, frenteSonarEcho)>25){     
        if(lerSonar(direitaSonarTrig,direitaSonarEcho) > 80){
          frente(200,200);
          delay(1000);
          dir++;
           // vira direita   
                  frente(0,0);
                 viraDireita(200,200,1000);
                 delay(1000);
                 frente(200,200);
                 delay(1000);     
        } else {
            estadoRobo=1; 
        }
      }
//----------------------------------------------------
    if(dir==2){ // esta a entrar no quarto 1
      estadoRobo=2;
      }
//---------------------------------------------------
    if (dir==3){ // esta a entrar quarto 2
      estadoRobo=3;
      }
//--------------------------------------------------
    if (dir=5){
      estadoRobo=6;
      }
      
break;
//--------------------------------------------------------- QUART O1 ------------------------------
  case 2: // quarto 1
     frente(200,200);
     if(lerSonar(frenteSonarTrig, frenteSonarEcho) < 25){
       estadoRobo=3;
        }else{
          estadoRobo=2;
          }

     if(fDireita==3){
      if(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) > 30){ // para sair do quarto 1
        frente(200,200);
          delay(1000);
        // vira esquerda
            fEsquerda++;
           frente(0,0);
           viraEsquerda(200,200,1000);
           delay(1000);
           frente(200,200);
          delay(1000);
          estadoRobo=1;
        }
      }

break;
//-------------------------------------- SONAR FRENTE -------------------------------

case 3: // // SONAR FRENTE 
    frente(0,0);
      if(lerSonar(direitaSonarTrig, direitaSonarEcho) < 20){        
             estadoRobo=4;        //compara sensores ESQ
      }else{  
        if(lerSonar(direitaSonarTrig, direitaSonarEcho) > 20){
                  //vira direita
                fDireita++;
                frente(0,0);
                 viraDireita(200,200,1000);
                 delay(1000);
                 frente(200,200);
                 delay(1000);
                 if(dir==2){
                  estadoRobo=2;
                  }else{
                      if(dir==3){
                        estadoRobo=5;
                        }
                        else{
                          if(dir==5){
                            estadoRobo=6;
                            }else{
                              if(quarto3==0){
                                estadoRobo=1;
                                }
                              }
                          }
                    }
                
      }
     
break;
  case 4: // SONAR FRENTE 
    frente(0,0);
      if(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) > 20){
         // virar ESQ
           fEsquerda++;
           frente(0,0);
           viraEsquerda(200,200,1000);
           delay(1000);
           frente(200,200);
          delay(1000);
          if(dir==2){
                  estadoRobo=2;
                  }else{
                      if(dir==3){
                        estadoRobo=5;
                        }
                        else{
                          if(dir==5){
                            estadoRobo=6;
                            }else{
                              if(quarto3==0){
                                estadoRobo=1;
                                }
                              }
                          }
                    }
        }
      else{  
        if(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) < 20){
        // rotate 180
          vc++; 
           frente(0,0);
           rodaCompleta(200,200,2000);
           delay(2000);
           frente(200,200);
          delay(1000);
         if(dir==2){
                  estadoRobo=2;
                  }else{
                      if(dir==3){
                        estadoRobo=5;
                        }
                        else{
                          if(dir==5){
                            estadoRobo=6;
                            }else{
                              if(quarto3==0){
                                estadoRobo=1;
                                }
                              }
                          }
                    } 
      }
      }
   
break;
// -------------------------------------------------------------quarto 02 -----------------------------------
  case 5: // quarto 2
frente(200,200);
     if(lerSonar(frenteSonarTrig, frenteSonarEcho) < 25){
       estadoRobo=3;
        }else{
          estadoRobo=5;
          }
if(fDireita==7){
      if(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho) > 30){ // para sair do quarto 2
        frente(200,200);
          delay(1000);
            // vira esquerda
            Esq++;
           frente(0,0);
           viraEsquerda(200,200,1000);
                delay(1000);
          frente(200,200);
          delay(1000);
          // vira esquerda
          Esq++;
           frente(0,0);
           viraEsquerda(200,200,1000);
                delay(1000);
          frente(200,200);
          delay(1000);
                   
          estadoRobo=1; // SAIR DO QUARTO 2
        }
      }

break;
// -------------------------------------------------------------quarto 03 -----------------------------------
  case 6:  // quarto 3
      frente(200,200);
      if(lerSonar(frenteSonarTrig, frenteSonarEcho) < 25){
       estadoRobo=3;
        }else{
          estadoRobo=6;
          }

       if (fEsquerda==3){
        estadoRobo=1;
        quarto3=1;
        
        }
break;

}




  
  

  //Movimento do robo
   Serial.println("Distancia: \t Frente \t Esquerda \t Direita");  
  Serial.print("\t \t");
  Serial.print(lerSonar(frenteSonarTrig, frenteSonarEcho));
  Serial.print("\t \t");
  Serial.print(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho));
  Serial.print("\t \t");
  Serial.println(lerSonar(direitaSonarTrig, direitaSonarEcho));
    //Bluetooth
    Bluetooth.println("Distancia: \t Frente \t Esquerda \t Direita");
    Bluetooth.print("\t \t");
    Bluetooth.print(lerSonar(frenteSonarTrig, frenteSonarEcho));
    Bluetooth.print("\t \t");
    Bluetooth.print(lerSonar(esquerdaSonarTrig, esquerdaSonarEcho));
    Bluetooth.print("\t \t");
    Bluetooth.println(lerSonar(direitaSonarTrig, direitaSonarEcho));
  
  //Funcao Chama
 
  Serial.print("Chama: ");
  //Bluetooth
  Bluetooth.print("Chama: ");
  Serial.println(analogRead(sensorChama));
  //Bluetooth
  Bluetooth.println(analogRead(sensorChama));
 detetaChama();
  //botoes

  
  Serial.print("Estado de Robo: ");
  Serial.println(estadoRobo);

 

      }
 }





