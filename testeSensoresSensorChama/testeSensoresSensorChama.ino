//Declaração pins e variáveis

//Sonar Pin(5,4,7,6,2,10)
char frenteSonarTrig = 5;
char frenteSonarEcho = 4;
char esquerdaSonarTrig = 7;
char esquerdaSonarEcho = 6;
char direitaSonarTrig = 2;
char direitaSonarEcho = 10;

//Sensor chama pin(A5)
int sensorChama = A5;

//Pin Motor
//A(3,12)
int motorAdir = 12;
int motorApwm = 3;

//B(11,13)
int motorBdir = 13;
int motorBpwm = 11;

//Fan pin(A2)
int fan = A2;

//LED vela detetada
int LED = 2;

//Declaração de variáveis utilizadas para controlo
double frente, esquerda, direita, distancia;
int valorChama , duracao, sensorChamaVal = 0, count = 0;
boolean botaoStart = false, botaoStop = false, chama = false;

void setup() {
  Serial.begin(9600);

  //Sonar frente
  pinMode(frenteSonarTrig, OUTPUT);
  pinMode(frenteSonarEcho, INPUT);

  //Sonar esquerda
  pinMode(esquerdaSonarTrig, OUTPUT);
  pinMode(esquerdaSonarEcho, INPUT);

  //Sonar direita
  pinMode(direitaSonarTrig, OUTPUT);
  pinMode(direitaSonarEcho, INPUT);

  //Fan
  pinMode(fan, OUTPUT);

  //Motores direito e esquerdo
  pinMode(motorAdir, OUTPUT);
  pinMode(motorApwm, OUTPUT);
  pinMode(motorBdir, OUTPUT);
  pinMode(motorBpwm, OUTPUT);

  //LED vela detetada
  pinMode(LED, OUTPUT);

}

void loop() {

  //Motor A
  digitalWrite(motorAdir, LOW);
  analogWrite(motorApwm, 255);

  //Motor B
  digitalWrite(motorBdir, HIGH);
  analogWrite(motorBpwm, 255);

  //Movimento do robo
  //Funcao Sonar frente
  frente = lerSonar(frenteSonarTrig, frenteSonarEcho);
  //End Funcao Sonar frente

  //Funcao Sonar esquerdo
  esquerda = lerSonar(esquerdaSonarTrig, esquerdaSonarEcho);
  //End Funcao Sonar esquerdo

  //Funcao Sonar direita
  direita = lerSonar(direitaSonarTrig, direitaSonarEcho);
  //End Funcao Sonar direita


  Serial.println("Distancia: \t Frente \t Esquerda \t Direita");

  Serial.print("\t \t");
  Serial.print(frente);
  Serial.print("\t \t");
  Serial.print(esquerda);
  Serial.print("\t \t");
  Serial.println(direita);

  //Funcao Chama
 // valorChama = lerChama();
  Serial.print(" - Chama: ");
 // Serial.println(valorChama);
 Serial.println(analogRead(sensorChama));
  ledChama();
  detetaChama();
}

void detetaChama() {
if (analogRead(sensorChama) > 400) {
    digitalWrite(fan, HIGH);    
    count++;
  }
  else  if (analogRead(sensorChama) <= 400) {
    digitalWrite(LED, LOW);
    digitalWrite(fan, LOW);
  }
}


void ledChama() {
  if (analogRead(sensorChama) > 400) {
    digitalWrite(LED, HIGH);
    delay(3000);
    digitalWrite(LED, LOW);
  } 
}

double lerSonar(char sonarTrig, char sonarEcho) {
  //Limpa o trigger
  digitalWrite(sonarTrig, LOW);
  delayMicroseconds(2);

  //Liga o trigger por 10 milisegundos
  digitalWrite(sonarTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(sonarTrig, LOW);

  //Recebe a informação (eccho)
  duracao = pulseIn(sonarEcho, HIGH);
  distancia = duracao * 0.034 / 2;

  return distancia;
}


/*int lerChama() {
  //Recebe a informação do sensor chama
  sensorChamaVal = analogRead(sensorChama);

  return sensorChamaVal;
}*/
